//     Signal | Noise - Backbone Example
//     Craig Richardson, craig.rich@hotmail.co.uk


//Define our namespace
var App = App || {};
App.Views = App.Views || {};
App.Models = App.Models || {};



//Example Backbone view
App.Views.User = Backbone.View.extend({

    //Define an element to bind this views events to.
    el: '#edit-user',

    events: {
        'click input[value=Save]': '_submit'
    },

    //Called when an instance of this view is created
    //Bind Model validation to callback
    initialize: function() {
        this._deserialise();
        this.model.on('invalid', this._handleError);
    },

    //Maps attributes to list of named inputs
    _deserialise: function() {
        var self = this;
        self.$('input[type=text]').each(function(index, el) {
            this.value = self.model.get(this.name) || '';
        });
    },

    //Maps named input values back to specific attributes
    _serialise: function() {
        var self = this;

        self.$('input[type=text]').each(function(index, el) {
            self.model.set(this.name, this.value);
        });
    },

    //callback for failed validation on model
    _handleError: function(model, err) {
        alert(err);
    },

    //serialises and submits model for saving
    _submit: function(e) {
        e.preventDefault();

        this._serialise();

        this.model.save()
            .done(function() {
                alert('Model updated!');
            })
            .fail(function() {
                alert('woops, something went wrong behind the scenes :(');
            });
    },
});



//Example of Backbone Model
App.Models.User = Backbone.Model.extend({

    //Placeholder data to make this example work
    urlRoot: 'http://jsonplaceholder.typicode.com/users',
    defaults: {
        id: 3,
        fname: 'Joe',
        lname: 'Bloggs',
        age: 30
    },

    //Called on model.save(), checks if all fields are filled and if age is a number
    validate: function(attrs) {

        var errors = [];

        _.each(attrs, function(attr, i) {
            if (!attr) errors.push('Please fill in all fields');
        });

        if (isNaN(attrs.age)) errors.push('Age has to be a number');

        //Return only the first error for demo
        if (errors.length) return errors[0];
    }
});



//Create new instance of view & model on document ready.
$(function() {
    var UserModel = new App.Models.User(),
        UserView = new App.Views.User({
            model: UserModel
        });
});


//Thoughts
//Could use a plugin such as stickitt to remove serialise / deserialise functions (offers 2 way data-binding)
//Could extend backbone save() to work with jquery promises correctly on failed validation.